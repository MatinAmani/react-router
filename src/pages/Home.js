const Home = () => {
    return (
        <div style={styles.root}>
            <h2>Home</h2>
            <div style={styles.contentContainer}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex ea
                explicabo natus doloremque odit unde quaerat quo doloribus
                debitis optio ducimus non nostrum, placeat quos veritatis
                delectus quod quidem sint. Lorem ipsum dolor sit amet
                consectetur adipisicing elit. Velit nihil assumenda corporis
                totam molestiae, quam aliquam magni est eaque tempore tempora.
                Obcaecati enim voluptas optio dignissimos nulla harum
                perferendis tenetur.
            </div>
        </div>
    )
}

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        border: '3px solid #333',
        borderRadius: '15px',
    },
    contentContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '10vh',
    },
}

export default Home
