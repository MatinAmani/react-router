const Docs = () => {
    return (
        <div style={styles.root}>
            <h2>Docs</h2>
            <div style={styles.contentContainer}>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Id
                recusandae quas dicta unde. Aspernatur explicabo quis eligendi
                nihil quasi. Quia, accusantium? Aspernatur fugit dolores
                architecto rem vitae sed consequatur cum. Lorem, ipsum dolor sit
                amet consectetur adipisicing elit. Labore eligendi expedita
                deleniti et, incidunt iure quia eum consectetur totam modi!
                Voluptate sint magni tempore fugiat facilis in sed veritatis
                laudantium.
            </div>
        </div>
    )
}

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        border: '3px solid #333',
        borderRadius: '15px',
    },
    contentContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '10vh',
    },
}

export default Docs
