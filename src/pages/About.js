const About = () => {
    return (
        <div style={styles.root}>
            <h2>About</h2>
            <div style={styles.contentContainer}>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Architecto doloribus ratione aperiam laudantium natus, eius id
                veritatis voluptatem corrupti tempore excepturi deserunt
                assumenda, possimus recusandae nostrum dolorum odit adipisci
                molestias? Lorem ipsum dolor sit amet consectetur adipisicing
                elit. Quam minus a consequuntur labore sit tenetur totam placeat
                dolor, cumque minima culpa in, exercitationem ipsam nam tempore
                esse ducimus, eaque reiciendis.
            </div>
        </div>
    )
}

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        border: '3px solid #333',
        borderRadius: '15px',
    },
    contentContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '10vh',
    },
}

export default About
