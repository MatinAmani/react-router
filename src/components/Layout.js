const Layout = ({ children }) => {
    return (
        <div>
            <head style={styles.headerContainer}>
                <h3>
                    <a href="/">Home</a> | <a href="/about">About</a> |{' '}
                    <a href="/docs">Docs</a>
                </h3>
            </head>
            <div style={styles.rootContainer}>{children}</div>
        </div>
    )
}

const styles = {
    rootContainer: {
        padding: '5vw',
    },
    headerContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
}

export default Layout
